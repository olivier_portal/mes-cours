# Leçon n°7

## Les variables

#### Poser une question:

utiliser la fonction prompt:

prompt("Please enter your name:", "Harry Potter");

#### Créer une variable

    let name = 'Olivier' (aussi var pour déclarer une variable mais plus d'actualité)

On peut soit rentrer en dur la valeur comme ci-dessus soit utiliser des fonctions

    
    let number = prompt(message: 'Donne moi un nombre');
    
    console.log(number);

on peut utiliser autat de variables que l'on veut, exemple:

    let number1 = 1;
    let number2 = 3;
    
    console.log(number1 + number2);