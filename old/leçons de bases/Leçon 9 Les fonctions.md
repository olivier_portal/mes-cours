# Leçon n°9

## Les fonctions

pour déclarer une fonction on utilise le mot clé function suivi du nom de la fonction  puis de paramètres entre parenthèses.
On ouvre ensuite des accolades dans lesquels on va intégrer ce que va faire la fonction

Il suffit ensuite d'appeler la fonction avec des valeurs (en dur ou définies dans des variables)

exemple, une fonction pour additionner des nombres et une pour soustraire:

    const number1 = 5;
    const number2 = 20;
    
    function addition(x, y) {
        console.log('Appel de la fonction addition', x + y)
    }
    
    function soustraction(x, y) {
        console.log('Appel de la fonction addition', x - y)
    }
    
    addition(number1, number2);
    addition(15, 35);
    
    soustraction(number2, number1);
    soustraction(10, 42);