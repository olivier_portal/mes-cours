# Leçon n°11

## Les objets

Pour utiliser les objets il sufffit d'ajouter des accolades dans la variable

**cela permet d'y assigné plusieurs variables et leurs valuers**

Pour séparé les valeurs il faut une virgule (nb utiliser le camelCasing pour nommer les différentes variables)

exemple, une fonction qui a pour objet mes coordonées perso:

    const me = {
     firstName: Olivier,
     lastName: Portal,
     age: 39,
     adress: '26 rue des fleurs'
     };
     
     
    console.log(me);
    
Pour n'afficher qu'une valeur en particulier on appelle l'objet + . +  nom de la variable, exemple:

    console.log(me.firstName);