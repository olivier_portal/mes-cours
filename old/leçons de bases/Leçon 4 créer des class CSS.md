# Leçon n°4

## Créer des classes CSS

d'abord créer la Balise dans la page html exemple: \<article>\</article> (pour créer une partie indépendante de la page)

On crée alors des class (dans la balise), ces class servent à pouvoir modifier le style de plusieurs balises en même temps peu importe le nom de la balise

#### mettre du style dans une class

Dans le fichier css afin d'utiliser la class, il faut l'écrire avec un point devant ex:

Dans le html on a la balise _article_ avec la class _card_ (\<article class="card">\</article>)

Afin d'ajouter du style à toutes les balises qui ont la class card, on écrit:

.card{
    color: red;
}

#### sélectioner uniquement une partie d'une section

par exemple dans le HTML:

\<article class="card">

   \<h2 class="card-title">Mon titre de la carte\</h2>
   
   \<p>lorem ipsum dolor sit met\</p>

\</article>

Pour la mise en forme on souhaite modifier la couleur du paragraphe contenu dans la balise article avec la class card
(et uniquement ce paragpahe, et pas les autres)

Dans le fichier CSS on écrit:

.card p{

  color: #444444;
  
  }