# Leçon n°5

## Intégrer du code Javascript à une page HTML

    Créer un fichier index.js
    
#### utiliser la console pour voir ce qui se passe dans son code

clic droit dans le navigateur -> inspecter -> ouvrir l'onglet console

dans le fichier index.js écrire l'instruction console.log

    exemple: console.log('Hello world !');
    
#### lier le fichier js à son html

Insérer la ligne de code suivante dans la balise head:

    <script src="index.js"></script>

_Astuce: pour ajouter le fichier index.js dans l'attribut **src** faire ctrl + espace l'IDE propose les fichiers_ existants

    les textes s'écrivent toujours avec des simple quote '', les chhiffres et bouléens sans quote