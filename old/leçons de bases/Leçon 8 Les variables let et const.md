# Leçon n°8

## Les variables let et const

Une variable dont la valeur ne sera assignée qu'une seule fois doivent être déclarée par **const**

Si la valeur de la varaible est amenée à être réassignée plusieurs fois  on utilisera **let**