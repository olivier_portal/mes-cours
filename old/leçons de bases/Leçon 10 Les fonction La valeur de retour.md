# Leçon n°10

## Les fonction: La valeur de retour

Pour utiliser les valeurs de retours on utilise le mot clé **return**

Il faut alors stocker le résultat de la fonction dans de nouvelles variables

exemple, une fonction pour additionner des nombres et une pour soustraire:

    const number1 = 5;
    const number2 = 20;
    
    function addition(x, y) {
        return  x + y;
    }
    
    function soustraction(x, y) {
        return  x - y;
    }
    
    const result = addition(number1, number2);    
    const result2 = soustraction(number2, number1);
    
    console.log('Appel de la fonction addition', result);
    console.log('Appel de la fonction addition', result2);