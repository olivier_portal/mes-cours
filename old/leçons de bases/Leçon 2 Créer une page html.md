# Leçon n°2

## Créer sa première page HTML

A partir de l'IDE créer un nouveau fichier **index.html** (toujours appeler la première page sous le nom index)

Cela crée automatiquement le début d'une structure correcte de la page HTML, à savoir:

    -> <!DOCTYPE HTML> ...
    
_Le HTML est un language MARKUP (HyperText Markup Language = html)_

## Les balises

Ce language est fait de balises qui s'écrivent par une balise ouvrante <> et fermante </> (nb: à l'exception de certaines balises qui ne se ferment pas)

> \<div class="Toto">Hello World!\</div>

Il faut toujours bien indenté son code afin de pouvoir s'y retrouver et pouvoir se relire, ou permettre à d'autres Dev de nous relire

Il suffit de faire 1 tab pour la 2éme balise dans la balise parent (chaque enfant peut-être parent d'une autre balise, rajouter un tab)

> Astuce: en cas de problème d'indentation, faire **Alt + Ctrl + l** pour réindenter correctement le fichier

##Les attributs

A l'intérieur de chaque balise il y a des attributs (ex: dans la balise HTML on retruve l'attribut lang="en" qui siggnifie que la page sera en anglais; fr pour français)

> ctrl + espace nous donne toutes les attributs possibles

#### Balises spéciales

Meta, Body, Title....

_Astuce: avec l'IDE webstorm quand on clique dans la page, l'IDE propose automatiquement le choix d'un navigateur afin de voir le résultat sur internet (on reste en local pour le moment_


#### Balises principales

\<h1>\</h1> pour le gros titre de la page
h2, 3, 4 pour les titres secondaires, tertiaires etc.

> astuce: Ne pas s'embêter avec les chevrons, écrire juste h1 + tab cela ajoute les chevrons pour la balise ouvrante et fermante

les paragraphes ont la balise p

