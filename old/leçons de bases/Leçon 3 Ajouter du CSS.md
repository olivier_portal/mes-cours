# Leçon n°3

## Ajouter du CSS à une page HTML

Le CSS **(cascade Sheet Style ou en français Feuille de style cascade)** cela ajoute le style (le jolie)

## Lier le CSS à la page HTML

A l'intérieur de la balise Head, ajouter la balise \<link rel="stylesheet" href="styles.css">

créer un fichier CSS (Atttention le nom du fichier doit s'appeller exactement comme indiquer dans la référence "href")

> astuce: pour ouvrir le fichier css à partir du fichier html faire contrôle + clic sur le "styles.css" dans l'attribut href

##Créer sa feuille de style 

reprendre le nom des balises et y ajouter des accolades, ex: **h1{}**

y ajouter des attributs comme par exemple la couleur:

h1{
    color: red;
   }
   
_les couleurs peuvent être des couleurs pré-enregistrée ex: red, green etc. ou en rgba (rouge vert bleu + opacité ou en exadécimal #444444_


Toujours finir une ligne par un **;**  **!!! important !!!**

