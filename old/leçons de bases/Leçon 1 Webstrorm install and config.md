# Leçon n°1

## Mettre en place l’environnement de travail pour coder en Javascript

### Mettre en place l'environnement Dev

Qu'est-ce qu'un environnement de Dev?

Source : [wikipedia](https://fr.wikipedia.org/wiki/Environnement_de_d%C3%A9veloppement)

> Dans un environnement de développement « intégré » (abrégé EDI en français ou IDE en anglais, pour integrated development environment)
>
>L'objectif d'un environnement de développement est d'augmenter la productivité des programmeurs en automatisant une partie des activités et en simplifiant les opérations. Les environnements de développement visent également à améliorer la qualité de la documentation en rapport avec le logiciel en construction5. Certains environnements de développement offrent également la possibilité de créer des prototypes, de planifier les travaux et de gérer des projets5.
>

### Téléchargement et configuration de **Webstorm**:

Allez sur le site de [Jetbrain](https://www.jetbrains.com/fr-fr/webstorm/) et télécharger l'IDE

_évitez de se faire mal aux yeux utiliser un thème sombre pour cela il faut changer le thème par défaut:_

allez dans configuration -> settings -> sélection le thème darcula

**Le plus important étant d'être productif et donc bien connaître ses raccourcis claviers etc.**

### Utilisation de l'IDE Webstorm:

Créer un nouveau projet (_nommer le de façon logique_)

Créer un répertoire puis créer un fichier dedans au format adéquate (.txt, .html ou comme ici .md pour la prise de note)

##### Quelques raccourcis importants afin de gagner en productivité:

>Shift + Fléche pour sélectionner un mot

>Shift + fin pour sélection toute une phrase (à partir de la position du curseur)

>Shift + début pour sélection toute une phrase (à partir de la position du curseur)

>ctrl + C V X A Z copier, coller, couper, tout sélectionner, revenir en arrière

>ctrl + shift + Z annule revenir en arrière

>ctrl + D dupliquer

>ctrl + alt + S ouvre la fenêtre de contrôle (on peut éditer ou ajouter des raccourcis)

>alt + j sélectionner une occurence (ex sélectionner tous les "+" du texte)

>ctrl + y supprimer une ligne

>shift + alt + flèche haut ou bas pour déplacer une ligne entière
