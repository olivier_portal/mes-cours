# Index:

## Recherche par mots clés :

###### Les opérateurs

[forEach](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/04-array.md#foreach) | 
[map](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/04-array.md#map)  | 
[filter](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/04-array.md#filter) | 
[sort](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/04-array.md#sort) | 
[push](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/04-array.md#push) | 
[concat](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/04-array.md#concat) | 
[splice](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/04-array.md#splice) | 
[reduce](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/04-array.md#reduce) | 
[find](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/04-array.md#find) | 
[every](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/04-array.md#every) | 
[some](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/04-array.md#some) | 
[toUpperCase](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/04-array.md#touppercase) | 
[replace](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/04-array.md#replace) | 

###### Les outils mathématiques

[Math.round](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/06-numbers%20manipulation.md#mathround) | 
[Math.floor](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/06-numbers%20manipulation.md#mathfloor) | 
[tofixed](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/06-numbers%20manipulation.md#tofixed) | 
[parseInt & parseFloat](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/06-numbers%20manipulation.md#parseint-et-parsefloat) | 
[Math.random](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/06-numbers%20manipulation.md#mathrandom) | 

###### Les requêtes au serveur

[listen](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/02-mettre%20en%20place%20node.md#ecouter-le-serveur) | 
[try-catch](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/04-le%20crud.md#try-catch) | 
[get](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/04-le%20crud.md#get) | 
[delete](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/04-le%20crud.md#delete) | 
[post](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/04-le%20crud.md#post) | 
[put](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/04-le%20crud.md#put) | 
[patch](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/04-le%20crud.md#patch) | 

###### Angular keywords

[interpolation](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/03-interpolations.md#interpolations) | 
[pipe](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/03-interpolations.md#les-pipes) | 
[ngFor](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/04-ng%20for.md#ng-for) | 
[ngIf](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/05-ng%20if%20et%20ng%20container.md#ngif) | 
[ngContainer](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/05-ng%20if%20et%20ng%20container.md#ngcontainer) | 
[input](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/06-les%20inputs.md#input) | 
[service](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/06-les%20inputs.md#les-services) | 
[click](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/07-click%20output%20et%20event%20emitter.md#cr%C3%A9er-un-click) | 
[event emitter](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/07-click%20output%20et%20event%20emitter.md#event-emitter) | 
[behavior subject](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/07-click%20output%20et%20event%20emitter.md#behavior-subject) | 
[unsubscribe](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/08-unsubscribe.md#unsubscribe) | 
[onDestroy](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/08-unsubscribe.md#ondestroy) | 
[takeUntil](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/08-unsubscribe.md#take-until) | 
[combineLatest](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/09-les%20appels%20au%20serveur.md#combinelatest) | 
[get](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/09-les%20appels%20au%20serveur.md#angular-get) | 
[post](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/09-les%20appels%20au%20serveur.md#post) | 
[delete](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/09-les%20appels%20au%20serveur.md#delete) | 
[put & patch](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/09-les%20appels%20au%20serveur.md#put-patch) | 
[form](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/10-form.md) | 
[form validation](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/10-form.md#form-validation) | 
[switchmap](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/11-switchmap.md) | 


## Recherche par languages :

* [markdown](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/markdown/markdown.md)

* [html](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/html/html.md)
    * [bootstrap (wireframe)](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/html/Bootstrap.md)
    
    
* [css](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/css/css.md)

* **javascript**
    * [introduction](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/01-introduction.md)
    * [organiser son code](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/02-organiser%20son%20code.md)
    * [function](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/03-function.md)
    * [array](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/04-array.md)
    * [object](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/05-object.md)
    * [numbers manipulation](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/06-numbers%20manipulation.md)
    * [boucle for](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/javascript/07-boucle%20for.md)
    
* **node**
     * [introduction au fonctionnement d'un serveur](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/01-introduction%20fonctionnement%20d'un%20serveur.md)
     * [mettre en place node](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/02-mettre%20en%20place%20node.md)
     * [administrer une db](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/03-administrer%20une%20base%20de%20donn%C3%A9es.md)
     * [le CRUD](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/04-le%20crud.md)
     * [les sous-collections](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/05-les%20sous%20collections.md)
     * [les enums](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/06-les%20enums.md#pourquoi-les-%C3%A9num%C3%A9rations-?)
     * [les classes](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/07-les%20classes.md)
     * [cross origin (cors)](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/08-cross%20origin%20(cors).md)
     * [créer un user avec une function](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/node/09-Cr%C3%A9er%20un%20user%20avecune%20function.md)
     
* **react**
    * [introduction à React](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/react/01-introduction%20%C3%A0%20React.md)
    * [JSX (obsolète)](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/react/02-JSX.md)
    * [créer des composants](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/react/03-cr%C3%A9er%20des%20composants.md)
    * [gérer le css](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/react/04-g%C3%A9rer%20le%20css.md)
    * [gérer les évènements](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/react/05-g%C3%A9rer%20les%20%C3%A9v%C3%A8nements.md)
    * [les formulaires](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/react/06-les%20formulaires.md)
    * [affichage conditionnel](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/react/07-affichage%20conditionnel.md)
    * [use effect](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/react/08-use%20effect.md)
    * [create react app](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/react/09-create%20react%20app.md)
    * [react router](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/react/10-react%20router.md)

* **angular**
    * [new angular project](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/01-new%20angular%20project.md)
    * [créer un module](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/02-cr%C3%A9er%20un%20module.md)
    * [les interpolations](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/03-interpolations.md)
    * [ngFor](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/04-ng%20for.md)
    * [ngIf & ngContainer](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/05-ng%20if%20et%20ng%20container.md)
    * [les inputs](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/06-les%20inputs.md)
    * [click output et event emitter](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/07-click%20output%20et%20event%20emitter.md)
    * [unsubscribe](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/08-unsubscribe.md)
    * [les appels au serveur](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/09-les%20appels%20au%20serveur.md)
    * [form](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/10-form.md)
    * [switchmap](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/11-switchmap.md)
    * [angularFire Authentification](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/12-angularFire%20Authentifcation.md)
    * [angularFire Créer un user (mail et mdp)](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/angular/13-angularFire%20cr%C3%A9er%20un%20user%20(mail%20et%20mdp).md)
     
* **typescript**
     * [le typage](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/typescript/01-Le%20typage.md)
     * [truthy and falsy](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/Languages/typescript/02-truthy%20and%20falsy.md)

## Recherche par tools :

* [gitlab](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/tools/git/Gitlab.md)
* [firebase](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/tools/firebase/firebase.md)
* [nexus](https://gitlab.com/olivier_portal/mes-cours/-/blob/master/tools/nexus/configurer%20un%20nexus%20perso.md)
